// comentario de uma linha
/*
comentario de bloco
*/

console.log('REPETIÇÃO WHILE');

let numero = 0;

while(numero<=10) {
    if(numero%2==0) {
        console.log(`O valor: ${numero} é par!`);
    } else {
        console.log(`O valor: ${numero} é impar!`);
    }
    numero++;
}

console.log('REPETIÇÃO DOWHILE');

let numero2 = 0;

do {
    if(numero2%2==0) {
        console.log(`O valor: ${numero2} é par!`);
    } else {
        console.log(`O valor: ${numero2} é impar!`);
    }
    numero2++; 

} while(numero2<=10);

console.log("REPETIÇÃO FOR");

let numero3 = 0;

for(let i = 0; i<=10; i++) {
    if(numero3%2==0) {
        console.log(`O valor: ${numero3} é par!`);
    } else {
        console.log(`O valor: ${numero3} é impar!`);
    }
    numero3++; 
}